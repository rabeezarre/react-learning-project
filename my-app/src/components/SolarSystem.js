import React from "react";
import "../styles/SolarSystem.css";

const title = React.createElement(
  "h1",
  {
    style: { color: "#999", fontSize: "19px" },
  },
  "Solar system planets:"
);

const planets = [
  "Mercury",
  "Venus",
  "Earth",
  "Mars",
  "Jupiter",
  "Saturn",
  "Uranus",
  "Neptune",
];

function changeTheme() {
  const body = document.querySelector('body');
  body.classList.toggle('dark');
  const list = document.getElementById('planets-list')
  list.classList.toggle('planets-list-dark');
}

export default function SolarSystemList() {
  return (
    <div>
      <div>
        {title}
        <ul id="planets-list">
          {planets.map((planet) => (
            <li key={planet}>{planet}</li>
          ))}
        </ul>
      </div>

      <label className="switch" htmlFor="checkbox">
        <input type="checkbox" id="checkbox" />
        <div className="slider round" onClick={changeTheme}></div>
      </label>
    </div>
  );
}
